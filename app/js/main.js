$(document).ready(function() {
    var innerPageSidebar = $('.inner-page__sidebar');
    var innerPageContentArea = $('.inner-page__content-area');
    var siteHeader = $('.site-header');
    var siteFooter = $('.site-footer');
    var navbar = $('.site-header__nav');
    var navListItem = $('.site-header__nav-list > li');
    var productNavList = $('.product-page .site-header__nav-list');
    var mobileBtn = $('.site-header__mobile-btn');

    // innerSidebarSticky
    $('.inner-page__sidebar').stickit({
        screenMinWidth: 1024,
        top: siteHeader.outerHeight() + parseFloat(innerPageContentArea.css('padding-top'))
        // top: siteHeader.outerHeight()
    });
    // END:innerSidebarSticky

    mobileBtn.on('click', function(event) {
        event.preventDefault();
        navbar.toggleClass('site-header__nav--active');
    });

    function checkPosition() {
        if (window.matchMedia('(min-width: 1201px)').matches) {
            navListItem.hover(function() {
                $(this).find('.submenu').addClass('active');
                $(this).siblings().find('.submenu').hide();
            }, function() {
                $(this).find('.submenu').removeClass('active');
                $(this).siblings().find('.submenu').show();
            });
        } else {
            $('.site-header__nav-list > li.current-menu-item').on('click', function(event) {
                // event.preventDefault();
                $(this).siblings().find('.submenu').slideUp();
                $(this).find('.submenu').slideToggle().css({
                    visibility: 'visible'
                });
            });
        }
    }
    checkPosition();
    $(window).resize(function(event) {
        checkPosition();
    });
});

$(document).on("mouseup", function(ev) {
    var sidebarNav = $('.site-header__nav');
    var mobileBtn = $('.site-header__mobile-btn');

    sidebarNav.is(ev.target) || mobileBtn.is(ev.target) || 0 !== sidebarNav.has(ev.target).length || (sidebarNav.removeClass("site-header__nav--active"),
        $(".sidebar-menu-overlay").removeClass("active", 1e3, "easeOutBounce"))
});
